	Copyright 2018 Alexandre D'HONT
	This file is part of mercator.

	Mercator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mercator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mercator.  If not, see <https://www.gnu.org/licenses/>.

from threading import Thread
import time

from sensor import imu, gps
from actuator import servo


class pid:
	def __init__(self):	
		self.value = 0.0
		self.target = 0.0
		self.error = self.target - self.value
		self.prev_error = 0.0
		self.sum_error = 0.0
		self.order = 0.0
		self.KP = 0.2
		self.KD = 0.01
		self.KI = 0.005

	def update(self):
		self.error = float(self.target - self.value)
		self.order = (self.error * self.KP) + ((self.error - self.prev_error) * self.KD) + (self.sum_error * self.KI)
		self.prev_error = self.error
		self.sum_error += self.error


class autoPilot(Thread):
	def __init__(self,period=1):
		Thread.__init__(self)
		self.period = period
		self.off = 0

		self.cap = 0
		self.cog = 0
		self.spd = 0
		self.ord = 0
		self.val = 0

		self.imu = imu()

		self.rudder = servo()
		self.rudder.channel = 1
		self.rudder.min = -30
		self.rudder.max = 30
		
		self.pid = pid()
		
		print ("Autopilot Initialized")
	
	def reset(self):
		self.pid.prev_error = 0
		self.pid.sum_error = 0

	def run(self):
		self.gps = gps()
		self.gps.start()
		print ("Autopilot Started")

		while True:
			if self.off == 1:
				self.gps.off = 1
				self.gps.join()
				break

			self.imu.get()		

			self.cog = round(self.gps.cog)

			self.spd = round(self.gps.spd,2)

			
			self.pid.value = self.cog
			
			if self.cap != self.pid.target:
				self.reset()
				self.pid.target = self.cap
			
			self.pid.update()
			self.ord = self.pid.order

			self.rudder.angle(-self.ord)
			self.val = self.rudder.value

						
			time.sleep(self.period)

			
