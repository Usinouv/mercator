	Copyright 2018 Alexandre D'HONT
	This file is part of mercator.

	Mercator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mercator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mercator.  If not, see <https://www.gnu.org/licenses/>.
	
from calculs import interpolate
import Adafruit_PCA9685

class servo:
	""
	def __init__(self):
		self.pwm = Adafruit_PCA9685.PCA9685()
		self.pwm.set_pwm_freq(50)

		self.minValue = 230
		self.maxValue = 400
		self.defValue = 315
		self.min = 0
		self.max = 100
		self.value = self.defValue
		self.channel = 0
	
	def set(self):
		self.pwm.set_pwm(self.channel,0,self.value)

	def reset(self):
		self.value = self.defValue
		self.set()

	def angle(self, angle):
		angle = max(min(angle,self.max),self.min)
		self.value = int( interpolate( (self.min,self.minValue),(self.max,self.maxValue),angle ))
		self.set()
