	Copyright 2018 Alexandre D'HONT
	This file is part of mercator.

	Mercator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mercator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mercator.  If not, see <https://www.gnu.org/licenses/>.

from threading import Thread
from gps3 import agps3
import time
import board
import busio
import lsm9ds1
import math
import os

RAD_TO_DEG = 57.29578
M_PI = 3.14159265358979323846

################# Compass Calibration values ############
# Use calibrateBerryIMU.py to get calibration values 
# Calibrating the compass isnt mandatory, however a calibrated 
# compass will result in a more accurate heading values.
magXmin =  -0.27594
magYmin =  -0.24962
magZmin =  -0.58562
magXmax =  0.3969
magYmax =  0.57974
magZmax =  0.30352

class imu:
	
	def __init__(self):
		# I2C connection:
		i2c = busio.I2C(board.SCL, board.SDA)
		self.imu = lsm9ds1.LSM9DS1_I2C(i2c)

		self.AccXangle = 0
		self.AccYangle = 0	
		self.magXcomp = 0
		self.magYcomp = 0
		self.heading = 0
		self.tiltCompensatedHeading = 0
		  
		
	def get(self):

		#Read the accelerometer,gyroscope and magnetometer values
		ACCx, ACCy, ACCz = self.imu.accelerometer
		MAGx, MAGy, MAGz = self.imu.magnetometer


		#Apply compass calibration    
		MAGx -= (magXmin + magXmax) /2 
		MAGy -= (magYmin + magYmax) /2 
		MAGz -= (magZmin + magZmax) /2 
	 
		##Convert Accelerometer values to degrees
		self.AccXangle =  (math.atan2(ACCy,ACCz)+M_PI)*RAD_TO_DEG
		self.AccYangle =  (math.atan2(ACCz,ACCx)+M_PI)*RAD_TO_DEG


		####################################################################
		######################Correct rotation value########################
		####################################################################
		#Change the rotation value of the accelerometer to -/+ 180 and
		#move the Y axis '0' point to up.
		#
		#Two different pieces of code are used depending on how your IMU is mounted.
		#If IMU is up the correct way, Skull logo is facing down, Use these lines
		self.AccXangle -= 180.0
		if self.AccYangle > 90:
		    self.AccYangle -= 270.0
		else:
		    self.AccYangle += 90.0

		############################ END ##################################


		#Calculate heading
		self.heading = 180 * math.atan2(MAGy,MAGx)/M_PI

		#Only have our heading between 0 and 360
		if self.heading < 0:
		    self.heading += 360



		####################################################################
		###################Tilt compensated heading#########################
		####################################################################
		#Normalize accelerometer raw values.
		self.accXnorm = ACCx/math.sqrt(ACCx * ACCx + ACCy * ACCy + ACCz * ACCz)
		self.accYnorm = ACCy/math.sqrt(ACCx * ACCx + ACCy * ACCy + ACCz * ACCz)
	   
		#Calculate pitch and roll
		#Use these two lines when the IMU is up the right way. Skull logo is facing down
		self.pitch = math.asin(self.accXnorm)
		self.roll = -math.asin(self.accYnorm/math.cos(self.pitch))

		#Calculate the new tilt compensated values
		self.magXcomp = MAGx*math.cos(self.pitch)+MAGz*math.sin(self.pitch)
		self.magYcomp = MAGx*math.sin(self.roll)*math.sin(self.pitch)+MAGy*math.cos(self.roll)+MAGz*math.sin(self.roll)*math.cos(self.pitch)   #LSM9DS1

		#Calculate tilt compensated heading
		self.tiltCompensatedHeading = 180 * math.atan2(self.magYcomp,self.magXcomp)/M_PI

		if self.tiltCompensatedHeading < 0:
		            self.tiltCompensatedHeading += 360

class gps(Thread):
	def __init__(self):
		Thread.__init__(self)
		self.lat = 0
		self.lon = 0
		self.spd = 0
		self.cog = 0
		self.off = 0

		self.gps_socket = agps3.GPSDSocket()
		self.data_stream = agps3.DataStream()
		self.gps_socket.connect()
		self.gps_socket.watch()


	def run(self):
		for new_data in self.gps_socket:
				if self.off == 1:
					break
				if new_data:
					self.data_stream.unpack(new_data)
					self.lat = self.data_stream.lat
					self.lon = self.data_stream.lon
					self.spd = self.data_stream.speed
					self.cog = self.data_stream.track

					if isinstance(self.lat,str):
						self.lat = 0.0
					if isinstance(self.lon,str):
						self.lon = 0.0
					if isinstance(self.spd,str):
						self.spd = 0.0
					if isinstance(self.cog,str):
						self.cog = 0.0
