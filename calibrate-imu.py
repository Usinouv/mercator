#!/usr/bin/python
#   This program is used to calibrate the compass on a BerryIMUv1 or
#   BerryIMUv2.
#
#   Start this program and rotate your BerryIMU in all directions. 
#   You will see the maximum and minimum values change. 
#   After about 30secs or when the values are not changing, press Ctrl-C.
#   The program will printout some text which you then need to add to
#   berryIMU.py or berryIMU-simple.py


import sys,signal,os
import time
import math
import board
import busio
import lsm9ds1

import datetime

# I2C connection:
i2c = busio.I2C(board.SCL, board.SDA)
sensor = lsm9ds1.LSM9DS1_I2C(i2c)

def handle_ctrl_c(signal, frame):
    print (" ")
    print ("magYmin = ",  magXmin)
    print ("magYmin = ",  magYmin)
    print ("magZmin = ",  magZmin)
    print ("magXmax = ",  magXmax)
    print ("magYmax = ",  magYmax)
    print ("magZmax = ",  magZmax)
    sys.exit(130) # 130 is standard exit code for ctrl-c

#This will capture exit when using Ctrl-C
signal.signal(signal.SIGINT, handle_ctrl_c)


a = datetime.datetime.now()


#Preload the variables used to keep track of the minimum and maximum values
magXmin = float(32767)
magYmin = float(32767)
magZmin = float(32767)
magXmax = float(-32767)
magYmax = float(-32767)
magZmax = float(-32767)


    
while True:

 
    
    #Read magnetometer values
    MAGx, MAGy, MAGz = sensor.magnetometer
    print(MAGx, MAGy, MAGz)
    
    
    if MAGx > magXmax:
        magXmax = MAGx
    if MAGy > magYmax:
        magYmax = MAGy
    if MAGz > magZmax:
        magZmax = MAGz

    if MAGx < magXmin:
        magXmin = MAGx
    if MAGy < magYmin:
        magYmin = MAGy
    if MAGz < magZmin:
        magZmin = MAGz

    print(" magXmin  %f  magYmin  %f  magZmin  %f  ## magXmax  %f  magYmax  %f  magZmax %f  " %(magXmin,magYmin,magZmin,magXmax,magYmax,magZmax))



    #slow program down a bit, makes the output more readable
    time.sleep(0.03)


