	Copyright 2018 Alexandre D'HONT
	This file is part of mercator.

	Mercator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mercator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mercator.  If not, see <https://www.gnu.org/licenses/>.

import time
import datetime
from threading import Thread

class log(Thread):
	def __init__(self,period=1):
		Thread.__init__(self)
		self.period = period

		self.cog = 0
		self.cap = 0
		self.order = 0
		self.speed = 0
		self.rudder = 0
		self.sail = 0
		self.windDir = 0
		self.windSpd = 0
		self.off = 0

		self.file = "./Log/Sail.{0}.log".format(time.strftime("%y_%m_%d.%H%M%S"))
		with open(self.file, "w") as log:
			title = "Time\tCap\tCoG\tOrder\tSpeed\trudder\tsail\tDir\tSpd\n"
			log.write(title)
		print ("Log Initalized")
		

	def run(self):
		print ("Log Started")

		with open(self.file, "w") as log:
			while True:
				if self.off == 1:
					break

				date = datetime.datetime.now()
				timestamp = "{0}:{1}:{2}.{3}".format(date.hour,date.minute,date.second,round(date.microsecond,2))
				msg = "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\n".format(timestamp,self.cap,self.cog,self.order,self.speed,self.rudder,self.sail,self.windDir,self.windSpd)
			
				log.write(msg)

				time.sleep(self.period)
