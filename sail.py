	Copyright 2018 Alexandre D'HONT
	This file is part of mercator.

	Mercator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Mercator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mercator.  If not, see <https://www.gnu.org/licenses/>.

from threading import Thread

from log import log
from regulator import autoPilot
from actuator import servo

rudder = servo()
rudder.channel = 1
rudder.min = -30
rudder.max = 30

sail = servo()
sail.channel = 2

wd, ws, cap = 0,0,0

log = log(0.5)


autoPilot = autoPilot(1)

while True:
	ask = input("Saisie : ").split(" ")

	if ask[0] == "q":
			rudder.reset()
			sail.reset()
			log.off = 1
			autoPilot.off = 1

			try:
				log.join()
			except RuntimeError:
				print("Log already stopped")
			else:
				print("Log stopped")
			
			try:
				autoPilot.join()
			except RuntimeError:
				print("Autopilot already stopped")
			else:
				print("Autopilot stopped")

			break
	elif ask[0] == "o":
			rudder.reset()
			sail.reset()
	elif ask[0] == "rr":
			rudder.reset()
	elif ask[0] == "ss":
			sail.reset()
	elif ask[0] == "ap":
		if ask[1] == "start":
			autoPilot.start()
		elif ask[1] == "stop":
			autoPilot.off = 1
			try:
				autoPilot.join()
			except RuntimeError:
				print("Autopilot already stopped")
			else:
				print("Autopilot stopped")
		elif ask[1] == "reset":
				autoPilot.reset()
	elif ask[0] == "lg":
		if ask[1] == "start":
			log.start()
		elif ask[1] == "stop":
			log.off = 1
			try:
				log.join()
			except RuntimeError:
				print("Log already stopped")
			else:
				print("Log stopped")
	elif ask[0] == "info":
		print ("Cap : ",cap)
		print ("CoG : ",autoPilot.cog)
		print ("Speed : ",autoPilot.spd)

	else:
		try:
			angle = int(ask[1])
		except IndexError:
			print("Mauvais nombre d'argument")
		except ValueError:
			print("Mauvais parametres")
		else:
			if ask[0] == "r":
				rudder.angle(angle)
			elif ask[0] == "s":
				sail.angle(angle)
			elif ask[0] == "d":
				wd = angle
			elif ask [0] == "w":
				ws = angle
			elif ask [0] == "c":
				cap = angle

	autoPilot.cap = cap

	log.cog = autoPilot.cog
	log.cap = cap
	log.order = autoPilot.ord
	log.speed = autoPilot.spd
	log.rudder = autoPilot.val
	log.sail = sail.value
	log.windDir = wd
	log.windSpd = ws
		
